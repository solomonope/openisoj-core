package org.bitbucket.openisoj.fieldvalidator;

public interface IFieldValidator {
	public String getDescription();

	public boolean isValid(String value);
}
